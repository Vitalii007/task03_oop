package com.epam;

import java.util.*;

public class CustomerService {

    private int key;
    private List<Accommodation> list = new ArrayList<Accommodation>();

    public final void accommodationList() {

        addElements(list);
        Scanner sc = new Scanner(System.in);
        do {
            outputMenu();
            key = sc.nextInt();
            switch (key) {
                case 0:
                    break;
                case 1:
                    showAccommodation(list);
                    break;
                case 2:
                    searchAccommodation();
                    break;
                case 3:
                    sortAccommodation();
                    break;
                default:
                    System.out.println("There are no such point in menu. "
                            + "Please try again.");
            }
        } while (key != 0);
    }

    public final void outputMenu() {
        System.out.println("MENU:");
        System.out.println("1 - see list of accommodations");
        System.out.println("2 - search accommodation by certain parameters");
        System.out.println("3 - sort accommodations by certain parameter");
        System.out.println("0 - quit");
        System.out.println("Please, choose menu point");
    }

    public final void showAccommodation(final List<Accommodation> example) {
        for (int i = 0; i < example.size(); i++) {
            System.out.println(example.get(i));
        }
    }

    public final void reverseShowAccomodation(
            final List<Accommodation> example) {
        for (int i = example.size() - 1; i >= 0; i--) {
            System.out.println(example.get(i));
        }
    }

    public final void searchAccommodation() {
        List<Accommodation> copyList = new ArrayList<Accommodation>();
        copyList.addAll(list);
        Scanner scannerInt = new Scanner(System.in);
        Scanner scannerString = new Scanner(System.in);
        Scanner scannerDouble = new Scanner(System.in);
        int amountParameters;
        int price;
        String city;
        double distance;
        System.out.println("List of parameters:");
        System.out.println("1 - price");
        System.out.println("2 - city");
        System.out.println("3 - distance to school");
        System.out.println("4 - distance to kindergarten");
        System.out.println("5 - distance to center of city");
        System.out.println("How many parameters will you search for?");
        amountParameters = scannerInt.nextInt();
        int[] parameters = new int[amountParameters];
        for (int i = 0; i < amountParameters; i++) {
            System.out.println("Choose parameter you want search for:");
            parameters[i] = scannerInt.nextInt();
            if (parameters[i] == 1) {
                System.out.println("Enter price:");
                price = scannerInt.nextInt();
                copyList = compare(copyList, price);
            } else if (parameters[i] == 2) {
                System.out.println("Enter city:");
                city = scannerString.nextLine();
                copyList = compare(copyList, city);
            } else if (parameters[i] == 3) {
                System.out.println("Enter distance to school:");
                distance = scannerDouble.nextDouble();
                copyList = compare(copyList, distance, parameters[i]);
            } else if (parameters[i] == 4) {
                System.out.println("Enter distance to kindergarten:");
                distance = scannerDouble.nextDouble();
                copyList = compare(copyList, distance, parameters[i]);
            } else if (parameters[i] == 5) {
                System.out.println("Enter distance to center of city:");
                distance = scannerDouble.nextDouble();
                copyList = compare(copyList, distance, parameters[i]);
            } else {
                System.out.println("There are no such parameter. "
                        + "Please try again.");
                i--;
            }
        }
        showAccommodation(copyList);
    }

    public final void sortAccommodation() {
        int parameter;
        int sortingOrder;
        Scanner scanner = new Scanner(System.in);
        System.out.println("List of parameters:");
        System.out.println("1 - price");
        System.out.println("2 - city");
        System.out.println("3 - distance to school");
        System.out.println("4 - distance to kindergarten");
        System.out.println("5 - distance to center of city");
        do {
            System.out.println("Choose parameter you want sort for:");
            parameter = scanner.nextInt();
            System.out.println("Choose order you want sort for:");
            System.out.println("1 - increase, 2 - decrease");
            sortingOrder = scanner.nextInt();
            switch (parameter) {
                case 1:
                    Collections.sort(list, new PriceComparator());
                    if (sortingOrder == 1) {
                        showAccommodation(list);
                    } else {
                        reverseShowAccomodation(list);
                    }
                    break;
                case 2:
                    Collections.sort(list, new CityComparator());
                    if (sortingOrder == 1) {
                        showAccommodation(list);
                    } else {
                        reverseShowAccomodation(list);
                    }
                    break;
                case 3:
                    Collections.sort(list, new DistanceSchoolComparator());
                    if (sortingOrder == 1) {
                        showAccommodation(list);
                    } else {
                        reverseShowAccomodation(list);
                    }
                    break;
                case 4:
                    Collections.sort(list,
                            new DistanceKindergartenComparator());
                    if (sortingOrder == 1) {
                        showAccommodation(list);
                    } else {
                        reverseShowAccomodation(list);
                    }
                    break;
                case 5:
                    Collections.sort(list, new DistanceCityCenterComparator());
                    if (sortingOrder == 1) {
                        showAccommodation(list);
                    } else {
                        reverseShowAccomodation(list);
                    }
                    break;
                default:
                    System.out.println("There are no such parameter. "
                            + "Please try again.");
            }
        } while ((parameter < 1) || (parameter > 5));
    }

    public final List compare(final List<Accommodation> example,
                              final int price) {
        Scanner scanner = new Scanner(System.in);
        int sign;
        System.out.println("Choose sign:");
        System.out.print("1 - <, 2 - >, 3 - =\n");
        do {
            sign = scanner.nextInt();
            switch (sign) {
                case 1:
                    for (int i = 0; i < example.size(); i++) {
                        if (example.get(i).price >= price) {
                            example.remove(i);
                            i--;
                        }
                    }
                    break;
                case 2:
                    for (int i = 0; i < example.size(); i++) {
                        if (example.get(i).price <= price) {
                            example.remove(i);
                            i--;
                        }
                    }
                    break;
                case 3:
                    for (int i = 0; i < example.size(); i++) {
                        if (example.get(i).price != price) {
                            example.remove(i);
                            i--;
                        }
                    }
                    break;
                default:
                    System.out.println("There are no such sign."
                            + " Please try again.");
            }
        } while ((sign < 1) || (sign > 3));
        return example;
    }

    public final List compare(final List<Accommodation> example,
                              final String city) {
        for (int i = 0; i < example.size(); i++) {
            if (!example.get(i).city.equals(city)) {
                example.remove(i);
                i--;
            }
        }
        return example;
    }

    public final List compare(final List<Accommodation> example,
                              final double distance, final int value) {
        Scanner scanner = new Scanner(System.in);
        int sign;
        System.out.println("Choose sign:");
        System.out.print("1 - <, 2 - >, 3 - =\n");
        do {
            sign = scanner.nextInt();
            switch (sign) {
                case 1:
                    for (int i = 0; i < example.size(); i++) {
                        if (value == 3) {
                            if (example.get(i).distanceToSchool >= distance) {
                                example.remove(i);
                                i--;
                            }
                        } else if (value == 4) {
                            if (example.get(i).distanceToKindergarten
                                    >= distance) {
                                example.remove(i);
                                i--;
                            }
                        } else {
                            if (example.get(i).distanceToCenterOfCity
                                    >= distance) {
                                example.remove(i);
                                i--;
                            }
                        }
                    }
                    break;
                case 2:
                    for (int i = 0; i < example.size(); i++) {
                        if (value == 3) {
                            if (example.get(i).distanceToSchool
                                    <= distance) {
                                example.remove(i);
                                i--;
                            }
                        } else if (value == 4) {
                            if (example.get(i).distanceToKindergarten
                                    <= distance) {
                                example.remove(i);
                                i--;
                            }
                        } else {
                            if (example.get(i).distanceToCenterOfCity
                                    <= distance) {
                                example.remove(i);
                                i--;
                            }
                        }
                    }
                    break;
                case 3:
                    for (int i = 0; i < example.size(); i++) {
                        if (value == 3) {
                            if (example.get(i).distanceToSchool
                                    != distance) {
                                example.remove(i);
                                i--;
                            }
                        } else if (value == 4) {
                            if (example.get(i).distanceToKindergarten
                                    != distance) {
                                example.remove(i);
                                i--;
                            }
                        } else {
                            if (example.get(i).distanceToCenterOfCity
                                    != distance) {
                                example.remove(i);
                                i--;
                            }
                        }
                    }
                    break;
                default:
                    System.out.println("There are no such sign. "
                            + "Please try again.");
            }
        } while ((sign < 1) || (sign > 3));
        return example;
    }

    public final void addElements(final List<Accommodation> example) {
        example.add(new Mansion(3500, "Lviv", 3, 2.2, 1.8, 20.2));
        example.add(new Penthouse(1200, "Lviv", 1.2, 0.7, 1.3));
        example.add(new Villa(5000, "Lviv", 50, 3.0, 5.2, 11.7));
        example.add(new Flat(700, "Lviv", 4, 0.3, 1.1, 1.5));
        example.add(new Flat(400, "Lviv", 3, 1.2, 0.5, 0.5));
        example.add(new Flat(250, "Lviv", 2, 0.2, 3.1, 2.5));
        example.add(new Flat(375, "Kyiv", 2, 0.6, 0.4, 0.9));
        example.add(new Villa(12000, "Kyiv", 100, 30.9, 28.9, 33.4));
        example.add(new Villa(7600, "Lviv", 76, 1.8, 15.2, 14.7));
        example.add(new Mansion(2800, "Lviv", 3, 3.2, 10.8, 12.0));
        example.add(new Mansion(7000, "Kyiv", 4, 4.0, 3.8, 13.8));
        example.add(new Mansion(2200, "Lviv", 2, 1.0, 3.3, 9.0));
        example.add(new Penthouse(900, "Lviv", 3.4, 2.0, 4.3));
        example.add(new Penthouse(1500, "Kyiv", 0.9, 3.5, 6.6));
        example.add(new Penthouse(1200, "Lviv", 0.2, 1.6, 2.2));
        example.add(new Penthouse(800, "Ivano-Frankivsk", 0.5, 0.7, 1.1));
        example.add(new Penthouse(1500, "Lviv", 0.4, 3.0, 0.1));
    }
}
