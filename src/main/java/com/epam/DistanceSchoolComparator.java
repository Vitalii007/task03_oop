package com.epam;

import java.util.Comparator;

public class DistanceSchoolComparator implements Comparator<Accommodation> {
    public final int compare(final Accommodation o1, final Accommodation o2) {
        return o1.distanceToSchool < o2.distanceToSchool
                ? -1 : o1.distanceToSchool > o2.distanceToSchool ? 1 : 0;
    }
}
