package com.epam;

public class Flat extends Accommodation {

    private int amountRoom;

    public Flat(final int price, final String city, final int amountRoom,
                final double distanceToSchool,
                final double distanceToKindergarten,
                final double distanceToCenterOfCity) {
        this.price = price;
        this.city = city;
        this.amountRoom = amountRoom;
        this.distanceToSchool = distanceToSchool;
        this.distanceToKindergarten = distanceToKindergarten;
        this.distanceToCenterOfCity = distanceToCenterOfCity;    }

    @Override
    public final String toString() {
        return "Flat{"
                + "amountRoom=" + amountRoom
                + ", price=" + price
                + ", city='" + city + '\''
                + ", distanceToSchool=" + distanceToSchool
                + ", distanceToKindergarten=" + distanceToKindergarten
                + ", distanceToCenterOfCity=" + distanceToCenterOfCity + '}';
    }
}
