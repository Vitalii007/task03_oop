package com.epam;

import java.util.Comparator;

public class CityComparator implements Comparator<Accommodation> {
    public final int compare(final Accommodation o1, final Accommodation o2) {
        return o1.city.compareToIgnoreCase(o2.city);
    }
}
