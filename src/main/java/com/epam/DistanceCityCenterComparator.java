package com.epam;

import java.util.Comparator;

public class DistanceCityCenterComparator implements Comparator<Accommodation> {
    public final int compare(final Accommodation o1, final Accommodation o2) {
        return o1.distanceToCenterOfCity < o2.distanceToCenterOfCity ? -1
                : o1.distanceToCenterOfCity > o2.distanceToCenterOfCity ? 1 : 0;
    }
}
