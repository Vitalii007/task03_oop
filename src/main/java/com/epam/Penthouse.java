package com.epam;

public class Penthouse extends Accommodation {

    public Penthouse(final int price, final String city,
                     final double distanceToSchool,
                     final double distanceToKindergarten,
                     final double distanceToCenterOfCity) {
        this.price = price;
        this.city = city;
        this.distanceToSchool = distanceToSchool;
        this.distanceToKindergarten = distanceToKindergarten;
        this.distanceToCenterOfCity = distanceToCenterOfCity;
    }

    @Override
    public final String toString() {
        return "Penthouse{"
                + "price=" + price
                + ", city='" + city + '\''
                + ", distanceToSchool=" + distanceToSchool
                + ", distanceToKindergarten=" + distanceToKindergarten
                + ", distanceToCenterOfCity=" + distanceToCenterOfCity + '}';
    }
}
