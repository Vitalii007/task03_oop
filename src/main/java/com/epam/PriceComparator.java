package com.epam;

import java.util.Comparator;

public class PriceComparator implements Comparator<Accommodation> {
    public final int compare(final Accommodation o1, final Accommodation o2) {
        return o1.price < o2.price ? -1 : o1.price == o2.price ? 0 : 1;
    }
}
