package com.epam;

import java.util.Comparator;

public class DistanceKindergartenComparator
        implements Comparator<Accommodation> {
    public final int compare(final Accommodation o1, final Accommodation o2) {
        return o1.distanceToKindergarten < o2.distanceToKindergarten ? -1
                : o1.distanceToKindergarten > o2.distanceToKindergarten ? 1 : 0;
    }
}
