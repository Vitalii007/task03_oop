package com.epam;

public class Accommodation {

    protected int price;
    protected String city;
    protected double distanceToSchool;
    protected double distanceToKindergarten;
    protected double distanceToCenterOfCity;

    public static void main(final String[] args) {
        CustomerService service = new CustomerService();
        service.accommodationList();
    }
}
