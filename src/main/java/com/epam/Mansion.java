package com.epam;

public class Mansion extends Accommodation {

    private int amountFloor;

    public Mansion(final int price, final String city, final int amountFloor,
                   final double distanceToSchool,
                   final double distanceToKindergarten,
                   final double distanceToCenterOfCity) {
        this.price = price;
        this.city = city;
        this.amountFloor = amountFloor;
        this.distanceToSchool = distanceToSchool;
        this.distanceToKindergarten = distanceToKindergarten;
        this.distanceToCenterOfCity = distanceToCenterOfCity;    }

    @Override
    public final String toString() {
        return "Mansion{"
                + "amountFloor=" + amountFloor
                + ", price=" + price
                + ", city='" + city + '\''
                + ", distanceToSchool=" + distanceToSchool
                + ", distanceToKindergarten=" + distanceToKindergarten
                + ", distanceToCenterOfCity=" + distanceToCenterOfCity + '}';
    }
}
