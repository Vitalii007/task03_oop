package com.epam;

public class Villa extends Accommodation {

    private double square;

    public Villa(final int price, final String city, final double square,
                 final double distanceToSchool,
                 final double distanceToKindergarten,
                 final double distanceToCenterOfCity) {
        this.price = price;
        this.city = city;
        this.square = square;
        this.distanceToSchool = distanceToSchool;
        this.distanceToKindergarten = distanceToKindergarten;
        this.distanceToCenterOfCity = distanceToCenterOfCity;
    }

    @Override
    public final String toString() {
        return "Villa{"
                + "square=" + square
                + ", price=" + price
                + ", city='" + city + '\''
                + ", distanceToSchool=" + distanceToSchool
                + ", distanceToKindergarten=" + distanceToKindergarten
                + ", distanceToCenterOfCity=" + distanceToCenterOfCity + '}';
    }
}
